package lt.justelio.BinanceTraderApi;

import com.binance.connector.client.impl.WebsocketClientImpl;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lt.justelio.BinanceTraderApi.data.CoinData;
import lt.justelio.BinanceTraderApi.data.Order;
import lt.justelio.BinanceTraderApi.data_stream.DataStreamer;
import lt.justelio.BinanceTraderApi.layout.Center;
import lt.justelio.BinanceTraderApi.layout.Left;
import lt.justelio.BinanceTraderApi.market.GridBot;

/**
 * JavaFX App
 */
public class App extends Application {
	public static CoinData data;
	private static WebsocketClientImpl client;
	public static Label label;
	public static BorderPane layout;
	private static Scene scene;

	@Override
	public void start(Stage stage) throws InterruptedException {
		client = new WebsocketClientImpl();

		// uncomment next 2 lines for enabling data subscription for roseusdt pair
		DataStreamer stream1 = new DataStreamer();
		stream1.streamData(client, "roseusdt");

		Button btn = new Button("Close connection");
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				client.closeAllConnections();
			}
		});
		layout = new BorderPane();
		Left.buildLayout("Table", 20, new Font("Arial", 20));
		layout.setLeft(Left.getLeft());
		layout.setCenter(Center.buildCenter("action"));
		scene = new Scene(layout, 1080, 720);
		stage.setScene(scene);
		stage.show();
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				// TODO Auto-generated method stub
				event.consume();
				client.closeAllConnections();
				stage.close();
			}
		});
		new GridBot("ROSEUSDT", 0.25, 0.21, 7, 77);
		GridBot.startBot();
	}

	public static void changeLabel(String msg) {
		layout.getChildren().remove(0);
		layout.getChildren().add(new Label(msg));
	}

	public static void addOrder(Order order) {
		GridBot.orderTableData.add(order);
	}

	public static void main(String[] args) {
		launch();
	}

}