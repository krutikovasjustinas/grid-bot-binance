package lt.justelio.BinanceTraderApi.layout;

import javafx.geometry.Insets;

import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;

import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import lt.justelio.BinanceTraderApi.data.Order;
import lt.justelio.BinanceTraderApi.market.GridBot;

import static javafx.geometry.Pos.CENTER;


public class Left {

	private static VBox layout;
	private static Label captionLabel;
	private static TableView<Order> table;

	

	@SuppressWarnings("unchecked")
	public static void buildLayout(String captionText, int spacing, Font f) {
		layout = new VBox(spacing);
		layout.setBackground(
				new Background(new BackgroundFill(Color.rgb(20, 250, 250), CornerRadii.EMPTY, Insets.EMPTY)));
		captionLabel = new Label(captionText);
		captionLabel.setFont(f);
		captionLabel.setTextAlignment(TextAlignment.JUSTIFY);
		layout.setAlignment(CENTER);
		table = new TableView<Order>();

		TableColumn<Order, String> orderCol = new TableColumn<Order, String>("Order");
		TableColumn<Order, Double> priceCol = new TableColumn<Order, Double>("Price"),
				quantityCol = new TableColumn<Order, Double>("Quantity");
		TableColumn<Order, Boolean> orderTypeCol = new TableColumn<Order, Boolean>("Order Type");

		orderCol.setCellValueFactory(new PropertyValueFactory<Order, String>("orderId"));
		priceCol.setCellValueFactory(new PropertyValueFactory<Order, Double>("price"));
		quantityCol.setCellValueFactory(new PropertyValueFactory<Order, Double>("quantity"));
		orderTypeCol.setCellValueFactory(new PropertyValueFactory<Order, Boolean>("buy"));

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.setItems(GridBot.orderTableData);
//		table.setRowFactory(tv -> new TableRow<Order>(){
//			@Override
//		    public void updateItem(Order item, boolean empty) {
//		        super.updateItem(item, empty) ;
//		        if (item.isBuy()) {
//		            setStyle("-fx-background-color: tomato;");
//		        } else {
//		            setStyle("");
//		        }
//		    }
//		});
		table.getColumns().addAll(orderCol, priceCol, quantityCol, orderTypeCol);
		table.setMinHeight(table.getMaxHeight());
		layout.getChildren().addAll(captionLabel, table);

	}

	public static VBox getLeft() {
		return layout;
	}

}
