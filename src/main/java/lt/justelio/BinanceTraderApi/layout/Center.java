package lt.justelio.BinanceTraderApi.layout;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lt.justelio.BinanceTraderApi.App;
import lt.justelio.BinanceTraderApi.data.Order;

public class Center {
	private static Button actionBtn;
	private static HBox hLayout;
	private static VBox vLayout1, vLayout2;

	public static HBox buildCenter(String btnText) {
		actionBtn = new Button(btnText);
		actionBtn.setOnAction(e -> {
			App.addOrder(new Order("999999999", 0.294, 99.87, true));
		});
		hLayout = new HBox(20);
		vLayout1 = new VBox(20);
		vLayout2 = new VBox(20);
		vLayout1.getChildren().add(actionBtn);
		
		
		
		hLayout.getChildren().addAll(vLayout1, vLayout2);
		return hLayout;
	}
}
