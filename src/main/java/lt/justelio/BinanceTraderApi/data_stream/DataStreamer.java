package lt.justelio.BinanceTraderApi.data_stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.connector.client.impl.WebsocketClientImpl;
import lt.justelio.BinanceTraderApi.data.CoinDataReader;

public class DataStreamer {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(DataStreamer.class);

	public void streamData(WebsocketClientImpl client, String pair) {
		client.tradeStream(pair, ((event) -> {
			CoinDataReader.translateJson(event);
		}));
	}


}
