package lt.justelio.BinanceTraderApi.market;

public class FeeCounter {
	private static double buyingFees;
	private static double sellingFees;
	private static boolean usingBnb;

	public static double getBuyingFees() {
		return buyingFees;
	}

	public static double getSellingFees() {
		return sellingFees;
	}

	public static void setBuyingFees(double fee) {
		buyingFees = fee / 100;
	}

	public static void setSellingFees(double fee) {
		sellingFees = fee / 100;
	}

	public static double countBuyingFees(double price) {
		return usingBnb ? price * buyingFees * 0.75 : price * buyingFees;
	}

	public static double countSellingFees(double price) {
		return usingBnb ? price * buyingFees * 0.75 : price * sellingFees;
	}

	public static void setUsingBnb(boolean using) {
		usingBnb = using;
	}

}
