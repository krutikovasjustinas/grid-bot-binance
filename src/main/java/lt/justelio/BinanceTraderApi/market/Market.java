package lt.justelio.BinanceTraderApi.market;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.binance.connector.client.exceptions.BinanceClientException;
import com.binance.connector.client.exceptions.BinanceConnectorException;
import com.binance.connector.client.impl.SpotClientImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import lt.justelio.BinanceTraderApi.config.PrivateConfig;
import lt.justelio.BinanceTraderApi.data.CoinDataReader;
import lt.justelio.BinanceTraderApi.data.Order;
import lt.justelio.BinanceTraderApi.data.json_reader.Json;

public class Market {
	public ArrayList<Order> getOpenOrders(String pair)
	{
		LinkedHashMap<String,Object> parameters = new LinkedHashMap<>();

        SpotClientImpl client = new SpotClientImpl(PrivateConfig.API_KEY, PrivateConfig.SECRET_KEY, PrivateConfig.BASE_URL);

        parameters.put("symbol","ROSEUSDT");

        try {
            String result = client.createTrade().getOpenOrders(parameters);
            System.out.println(result);
            return CoinDataReader.translateOrdersJson(result);

        }
        catch (BinanceConnectorException e) {
           e.printStackTrace();
        }
        catch (BinanceClientException e) {
            e.printStackTrace();
        }
		return null;
	}
	public static Order createOrder(Order order, String pair)
	{
		LinkedHashMap<String,Object> parameters = new LinkedHashMap<>();

        SpotClientImpl client = new SpotClientImpl(PrivateConfig.API_KEY, PrivateConfig.SECRET_KEY, PrivateConfig.BASE_URL);

        System.out.println("pair: " + pair.toUpperCase());
        System.out.println("side: " + (order.isBuy() ?  "BUY" : "SELL"));
        System.out.println("quantity: " + order.getQuantity());
        System.out.println("price: " + order.getPrice());
        parameters.put("symbol", pair.toUpperCase());
        parameters.put("side", order.isBuy() ?  "BUY" : "SELL");
        parameters.put("type", "LIMIT");
        parameters.put("timeInForce", "GTC");
        parameters.put("quantity", order.getQuantity());
        parameters.put("price", order.getPrice());

        try {
            String result = client.createTrade().newOrder(parameters);
            JsonNode node = Json.parse(result);
            order.setOrderId(node.get("orderId").asText());
           
            System.out.println("Creating trade results:");
            System.out.println(result + "\n new order id: " + order.getOrderId());
        }
        catch (BinanceConnectorException e) {
            e.printStackTrace();
        }
        catch (BinanceClientException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println(order.toString());
        return order;
	}
	public static Order createMarketOrder(Order order, String pair)
	{
		LinkedHashMap<String,Object> parameters = new LinkedHashMap<>();

        SpotClientImpl client = new SpotClientImpl(PrivateConfig.API_KEY, PrivateConfig.SECRET_KEY, PrivateConfig.BASE_URL);

        parameters.put("symbol", pair.toUpperCase());
        parameters.put("side", order.isBuy() ?  "BUY" : "SELL");
        parameters.put("type", "MARKET");
        parameters.put("quantity", order.getQuantity());

        try {
            String result = client.createTrade().newOrder(parameters);
            JsonNode node = Json.parse(result);
            order.setOrderId(node.get("orderId").asText());
            System.out.println("Creating trade results:");
            System.out.println(result);
        }
        catch (BinanceConnectorException e) {
            e.printStackTrace();
        }
        catch (BinanceClientException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println(order.toString());
        return order;
	}
	
}
