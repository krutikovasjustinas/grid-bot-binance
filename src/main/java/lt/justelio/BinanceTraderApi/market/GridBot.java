package lt.justelio.BinanceTraderApi.market;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.binance.connector.client.exceptions.BinanceClientException;
import com.binance.connector.client.exceptions.BinanceConnectorException;
import com.binance.connector.client.impl.SpotClientImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lt.justelio.BinanceTraderApi.App;
import lt.justelio.BinanceTraderApi.config.PrivateConfig;
import lt.justelio.BinanceTraderApi.data.CoinBalance;
import lt.justelio.BinanceTraderApi.data.Order;
import lt.justelio.BinanceTraderApi.data.json_reader.Json;

public class GridBot {
	@SuppressWarnings("unused")
	private static String pair;
	public static double stepInUsdt;
	@SuppressWarnings("unused")
	private static double maxPrice, minPrice;
	private static int grids;
	@SuppressWarnings("unused")
	private static double usdtInUse;
	@SuppressWarnings("unused")
	private static double usdtStart;
	@SuppressWarnings("unused")
	private static double usdtNow;
	private static double pricePerOrder;
	public static ArrayList<Order> orders;
	public static ObservableList<Order> orderTableData = FXCollections.observableArrayList();
	public static WaitingForPrice prices = new WaitingForPrice();
	public static HashMap<String, CoinBalance> balance = new HashMap<String, CoinBalance>();
	public static boolean gridReady = false;

	public GridBot(String p, double max, double min, int g, double investment) {

		if ((investment / g) / min > 10) {
			pricePerOrder = investment / g;
			pair = p;
			stepInUsdt = (max - min) / g;
			grids = g;
			maxPrice = max;
			minPrice = min;
			usdtStart = investment;
			orders = new ArrayList<Order>();
		} else {
			System.out.println("grid parameters wrong!");
		}
	}

	public static void startBot() {
		DecimalFormat df = new DecimalFormat("0.00");
		balance = CoinBalance.getBalances();
		boolean minp = false;
		waitForInitializationComplete(2000);
		boolean lastBuy = false;
		Order ord = null;
		for (int i = 0; i < grids+1; i++) {
			double priceNow = App.data.getPrice();
			double gridPrice = minPrice + (i * stepInUsdt);
			double quantity = Double.parseDouble(df.format(pricePerOrder / gridPrice));
			if (i == 0 && priceNow < gridPrice) {
				minp = true;
				grids++;
			} else {
				if (priceNow < gridPrice) {//sell orders
					if(!lastBuy)
					{
						if (balance.get("ROSE").getFree() < quantity) {
							Order or = new Order(priceNow, quantity, true);
							Market.createMarketOrder(or, "ROSEUSDT");
						}
						Order o = new Order(gridPrice, quantity, false);
						ord = Market.createOrder(o, "ROSEUSDT");
						orders.add(ord);
					}
					lastBuy = false;
				} else {//buy orders
					if (balance.get("USDT").getFree() < quantity * priceNow) {
						Order or = new Order(priceNow, quantity, false);
						Market.createMarketOrder(or, "ROSEUSDT");
					}
					Order o = new Order(gridPrice, quantity, true);
					ord = Market.createOrder(o, "ROSEUSDT");
					orders.add(ord);
					lastBuy = true;
					balance = CoinBalance.getBalances();
				}
			}
		}
		if (minp)
			grids--;
		findNewWaits();
		System.out.println("Waiting for: " + prices.getLowerPrice() + "$ and " + prices.getUpperPrice() + "$");
		gridReady = true;
	}

	public static void findNewWaits() {
		if (orders.size() < 2)
			throw new ArrayIndexOutOfBoundsException();

		Order orLow = orders.get(0), orHigh = orders.get(orders.size() - 1);

		for (Order o : orders) {
			if (o.getPrice() > orLow.getPrice() && o.isBuy())
				orLow = o;
			else if (o.getPrice() < orHigh.getPrice() && !o.isBuy())
				orHigh = o;
		}
		prices.setLowerPrice(orLow.getPrice());
		prices.setUpperPrice(orHigh.getPrice());
	}

	private static void waitForInitializationComplete(int time) {
		while (App.data == null) {
			System.out.println("app.data is null, sleeping for " + time + "ms...");

			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unused")
	private static ArrayList<Order> getAllOrders() {
		ArrayList<Order> orders = new ArrayList<Order>();
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();

		SpotClientImpl client = new SpotClientImpl(PrivateConfig.API_KEY, PrivateConfig.SECRET_KEY,
				PrivateConfig.BASE_URL);

		parameters.put("symbol", "ROSEUSDT");

		try {
			String result = client.createTrade().getOpenOrders(parameters);
			JsonNode arrayOfOrders = Json.parse(result);
			for (int i = 0; i < arrayOfOrders.size(); i++) {
				JsonNode order = arrayOfOrders.get(i);
				Order o = new Order(order.get("orderId").asText(), order.get("price").asDouble(),
						order.get("quantity").asDouble(),
						order.get("side").asText().equalsIgnoreCase("buy") ? true : false);
				orderTableData.add(o);
			}
		} catch (BinanceConnectorException e) {
			e.printStackTrace();
		} catch (BinanceClientException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return orders;
	}

}
