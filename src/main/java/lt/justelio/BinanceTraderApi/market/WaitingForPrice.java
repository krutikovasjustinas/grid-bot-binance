package lt.justelio.BinanceTraderApi.market;

import lt.justelio.BinanceTraderApi.data.Order;

public class WaitingForPrice {
	private static double upperPrice;
	private static double lowerPrice;
	public double getLowerPrice()
	{
		return lowerPrice;
	}
	public double getUpperPrice()
	{
		return upperPrice;
	}
	public void setLowerPrice(double lowerPricee)
	{
		lowerPrice = lowerPricee;
	}
	public void setUpperPrice(double upperPricee)
	{
		upperPrice = upperPricee;
	}
	public static void checkForChanges(double price)
	{
		if(lowerPrice <= price)
		{
			System.out.print(lowerPrice + " < " + price + "     ");
		}
		else {
			System.out.println("found one!");
		}
		if(upperPrice >= price)
		{
			System.out.print(upperPrice + " > " + price + "\n");
		}
		else {
			System.out.println("FOUND ONE!");
		}
		
		if(price > upperPrice)
		{
			double step = 0.1;
			int indexOfOrder = Order.findOrder(GridBot.orders, upperPrice);
			Order oldOrd = GridBot.orders.get(indexOfOrder);
			GridBot.orders.remove(indexOfOrder);
			double gotFromOrder = (oldOrd.getPrice()*oldOrd.getQuantity());
			double nextPrice = upperPrice - GridBot.stepInUsdt;
			double nextQuantity = 10;
			while(true)
			{
				nextQuantity+=step;
				if(nextQuantity * nextPrice >= gotFromOrder)
				{
					nextQuantity-=step;
					break;
				}
			}
			System.out.println("I made another " + (nextQuantity - oldOrd.getQuantity() + " ROSE"));
			Order o = new Order(nextPrice, nextQuantity, true);
			Order ord = Market.createOrder(o, "ROSEUSDT");
			GridBot.orders.add(ord);
			GridBot.findNewWaits();
		}
		else if(price < lowerPrice){
			int indexOfOrder = Order.findOrder(GridBot.orders, lowerPrice);
			Order oldOrd = GridBot.orders.get(indexOfOrder);
			GridBot.orders.remove(indexOfOrder);
			double nextQuantity = oldOrd.getQuantity();
			double nextPrice = lowerPrice + GridBot.stepInUsdt;
			
			System.out.println("I bought " + nextQuantity + " ROSE");
			Order o = new Order(nextPrice, nextQuantity, false);
			Order ord = Market.createOrder(o, "ROSEUSDT");
			GridBot.orders.add(ord);
			GridBot.findNewWaits();
		}
	}
}
