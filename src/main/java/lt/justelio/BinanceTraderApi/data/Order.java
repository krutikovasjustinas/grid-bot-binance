package lt.justelio.BinanceTraderApi.data;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Order {
	private String orderId;
	private double price;
	private double quantity;
	private boolean buy;

	public Order(String orderId, double price, double quantity, boolean buy) {
		super();
		this.orderId = orderId;
		DecimalFormat df = new DecimalFormat("0.000");
		this.price = Double.parseDouble(df.format(price));
		this.quantity = Double.parseDouble(df.format(quantity));
		this.buy = buy;
	}
	public Order(double price, double quantity, boolean buy) {
		super();
		DecimalFormat df = new DecimalFormat("0.000");
		this.price = Double.parseDouble(df.format(price));
		df = new DecimalFormat("0.0");
		this.quantity = Double.parseDouble(df.format(quantity));
		this.buy = buy;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		DecimalFormat df = new DecimalFormat("0.000");
		this.price = Double.parseDouble(df.format(price));
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		DecimalFormat df = new DecimalFormat("0.0");
		this.quantity = Double.parseDouble(df.format(quantity));
	}

	public boolean isBuy() {
		return buy;
	}

	public void setBuy(boolean buy) {
		this.buy = buy;
	}
	
	public static int findOrder(ArrayList<Order> o, double price)
	{
		int orderId = 10000;
		for(int i = 0; i < o.size(); i++)
		{
			if(o.get(i).getPrice()==price)
			{
				orderId = i;
				break;
			}
		}
		return orderId;
	}
	

}
