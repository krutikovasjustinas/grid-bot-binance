package lt.justelio.BinanceTraderApi.data;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.binance.connector.client.exceptions.BinanceClientException;
import com.binance.connector.client.exceptions.BinanceConnectorException;
import com.binance.connector.client.impl.SpotClientImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;

import lt.justelio.BinanceTraderApi.config.PrivateConfig;
import lt.justelio.BinanceTraderApi.data.json_reader.Json;



public class CoinBalance {
	private double free;
	private double locked;
	
	public CoinBalance(double f, double l)
	{
		free = f;
		locked = l;
	}
	
	public void setFree(double f)
	{
		free = f;
	}
	public void setLocked(double l)
	{
		locked = l;
	}
	public double getFree()
	{
		return free;
	}
	public double getLocked() {
		return locked;
	}
	
	public static HashMap<String, CoinBalance> getBalances()
	{
		HashMap<String, CoinBalance> h = new HashMap<String, CoinBalance>();
		Logger logger = LoggerFactory.getLogger(CoinBalance.class);
	    
	        LinkedHashMap<String,Object> parameters = new LinkedHashMap<>();

	        SpotClientImpl client = new SpotClientImpl(PrivateConfig.API_KEY, PrivateConfig.SECRET_KEY, PrivateConfig.BASE_URL);

	        try {
	            String result = client.createTrade().account(parameters);
	            JsonNode json = Json.parse(result);
	            JsonNode balances = json.get("balances");
	            for(int i = 0; i < balances.size(); i++)
	            {
	            	JsonNode obj =  balances.get(i);
	            	h.put(obj.get("asset").asText(), new CoinBalance(obj.get("free").asDouble(), obj.get("locked").asDouble()));
	            }
	        }
	        catch (BinanceConnectorException e) {
	            logger.error("fullErrMessage: {}", e.getMessage(), e);
	        }
	        catch (BinanceClientException e) {
	            logger.error("fullErrMessage: {} \nerrMessage: {} \nerrCode: {} \nHTTPStatusCode: {}",
	                    e.getMessage(), e.getErrMsg(), e.getErrorCode(), e.getHttpStatusCode(), e);
	        } catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return h;
	}
}
