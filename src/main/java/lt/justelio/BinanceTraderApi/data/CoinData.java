package lt.justelio.BinanceTraderApi.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.application.Platform;
import lt.justelio.BinanceTraderApi.App;
import lt.justelio.BinanceTraderApi.market.GridBot;
import lt.justelio.BinanceTraderApi.market.WaitingForPrice;

public class CoinData {
	private String eventType;
	private long eventTime;
	private String tradeSymbol;
	private long tradeId;
	private double price;
	private double quantity;
	private long buyerId;
	private long sellerId;
	private long tradeTime;
	private boolean buyerMarketMaker;

	public CoinData() {
	}

	public CoinData(String eventType, long eventTime, String tradeSymbol, long tradeId, double price, double quantity,
			long buyerId, long sellerId, long tradeTime, boolean buyerMarketMaker) {
		super();
		this.eventType = eventType;
		this.eventTime = eventTime;
		this.tradeSymbol = tradeSymbol;
		this.tradeId = tradeId;
		this.price = price;
		this.quantity = quantity;
		this.buyerId = buyerId;
		this.sellerId = sellerId;
		this.tradeTime = tradeTime;
		this.buyerMarketMaker = buyerMarketMaker;

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				App.changeLabel("[" + convertDate(eventTime) + "] Price: " + price + " Quantity: " + quantity);
			}
		});
	}

	private String convertDate(long unix) {
		Date date = new Date(unix);
		SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String java_date = jdf.format(date);
		return java_date;
	}

	public String getEventType() {
		return eventType;
	}

	public long getEventTime() {
		return eventTime;
	}

	public String getTradeSymbol() {
		return tradeSymbol;
	}

	public long getTradeId() {
		return tradeId;
	}

	public double getPrice() {
		return price;
	}

	public double getQuantity() {
		return quantity;
	}

	public long getBuyerId() {
		return buyerId;
	}

	public long getSellerId() {
		return sellerId;
	}

	public long getTradeTime() {
		return tradeTime;
	}

	public boolean isBuyerMarketMaker() {
		return buyerMarketMaker;
	}
	

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public void setEventTime(long eventTime) {
		this.eventTime = eventTime;
	}

	public void setTradeSymbol(String tradeSymbol) {
		this.tradeSymbol = tradeSymbol;
	}

	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}

	public void setPrice(double price) {
		this.price = price;
		if(GridBot.gridReady)
			WaitingForPrice.checkForChanges(price);
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public void setBuyerId(long buyerId) {
		this.buyerId = buyerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public void setTradeTime(long tradeTime) {
		this.tradeTime = tradeTime;
	}

	public void setBuyerMarketMaker(boolean buyerMarketMaker) {
		this.buyerMarketMaker = buyerMarketMaker;
	}

	@Override
	public String toString() {
		return "CoinData [eventType=" + eventType + ", eventTime=" + eventTime + ", tradeSymbol=" + tradeSymbol
				+ ", tradeId=" + tradeId + ", price=" + price + ", quantity=" + quantity + ", buyerId=" + buyerId
				+ ", sellerId=" + sellerId + ", tradeTime=" + tradeTime + ", buyerMarketMaker=" + buyerMarketMaker
				+ "]";
	}

}
