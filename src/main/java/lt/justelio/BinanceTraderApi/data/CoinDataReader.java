package lt.justelio.BinanceTraderApi.data;


import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import lt.justelio.BinanceTraderApi.App;
import lt.justelio.BinanceTraderApi.data.json_reader.Json;

public class CoinDataReader {
	public static void translateJson(String rawData) {
		
		try {
			JsonNode node = Json.parse(rawData);
			if(App.data == null)
			{
				App.data = new CoinData(node.get("e").asText(), node.get("E").asLong(), node.get("s").asText(), node.get("t").asLong(), 
					node.get("p").asDouble(), node.get("q").asDouble(), node.get("b").asLong(), node.get("a").asLong(), node.get("T").asLong(),
					node.get("m").asBoolean());
				System.out.println("first api response: " + rawData);
			}
			else {
				App.data.setEventType(node.get("e").asText());
				App.data.setEventTime(node.get("E").asLong());
				App.data.setTradeSymbol(node.get("s").asText());
				App.data.setTradeId(node.get("t").asLong());
				App.data.setPrice(node.get("p").asDouble());
				App.data.setQuantity(node.get("q").asDouble());
				App.data.setBuyerId(node.get("b").asLong());
				App.data.setSellerId(node.get("a").asLong());
				App.data.setTradeTime(node.get("T").asLong());
				App.data.setBuyerMarketMaker(node.get("m").asBoolean());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static ArrayList<Order> translateOrdersJson(String rawData)
	{
		
		return null;
	}
}
