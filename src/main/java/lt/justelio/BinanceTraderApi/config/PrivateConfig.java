package lt.justelio.BinanceTraderApi.config;

public class PrivateConfig {
	public static final String API_KEY = "secret";
    public static final String SECRET_KEY = "secret";
    public static final String BASE_URL = "https://api.binance.com";
    public static final String TESTNET_API_KEY = "";
    public static final String TESTNET_SECRET_KEY = "";
}
